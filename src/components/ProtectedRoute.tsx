import { ROUTES } from 'constants/routes';
import { ReactNode } from 'react';
import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';
import { selectIsAuthenticated } from 'store/slices/auth';

export function ProtectedRoute({ children }: { children: ReactNode }) {
  const isAuthenticated = useSelector(selectIsAuthenticated);

  return isAuthenticated ? (children as JSX.Element) : <Navigate to={ROUTES.welcome} />;
}
