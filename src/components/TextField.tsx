import { Report } from '@mui/icons-material';
import { InputAdornment, TextField as TextFieldM, TextFieldProps } from '@mui/material';
import { ForwardedRef, forwardRef } from 'react';

export const TextField = forwardRef((props: TextFieldProps, ref: ForwardedRef<HTMLDivElement>) => {
  return (
    <TextFieldM
      {...props}
      ref={ref}
      InputProps={{
        endAdornment: props.error ? (
          <InputAdornment position="end">
            <Report color="error" />
          </InputAdornment>
        ) : null,
        ...props.InputProps,
      }}
    />
  );
});
