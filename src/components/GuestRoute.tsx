import { ROUTES } from 'constants/routes';
import { ReactNode } from 'react';
import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';
import { selectIsAuthenticated } from 'store/slices/auth';

export function GuestRoute({ children }: { children: ReactNode }) {
  const isAuthenticated = useSelector(selectIsAuthenticated);

  return isAuthenticated ? <Navigate to={ROUTES.home} /> : (children as JSX.Element);
}
