import { render, screen } from '@testing-library/react';
import App from '../app/App';

describe('App', () => {
  test('Should render without crashing 🤌', async () => {
    render(<App />);

    const greetingText = await screen.findByText('Cybellum Exercise');

    expect(greetingText).toBeInTheDocument();
  });
});
