import { Box, Link, Typography } from '@mui/material';
import illustrationgImage from 'assets/images/imac_dig_twins.png';
import logo from 'assets/images/logo.svg';
import Styled from './components/login.styled';
import { LoginForm } from './components/LoginForm';

export const LoginPage = () => {
  return (
    <Styled.GridLayout>
      <Styled.FormContainer>
        <Box component="img" src={logo} alt="" mb={4} width={150} />
        <Typography variant="h1" mb={7}>
          Welcome to the Product Security Platform
        </Typography>
        <LoginForm />
      </Styled.FormContainer>
      <Styled.IllustrationContainer>
        <img src={illustrationgImage} alt="" />
      </Styled.IllustrationContainer>
      <Styled.Footer>
        <Link href="#">Privacy policy</Link>
        <Link href="#">Terms of use</Link>
        <Link href="#">Contact us</Link>
      </Styled.Footer>
    </Styled.GridLayout>
  );
};
