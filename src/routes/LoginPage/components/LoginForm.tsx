import { zodResolver } from '@hookform/resolvers/zod';
import { Box, Button, Link } from '@mui/material';
import { TextField } from 'components/TextField';
import { useForm } from 'react-hook-form';
import { Link as RouterLink } from 'react-router-dom';
import { isErrorResponseWithMessage } from 'store/services/api';
import { useLoginMutation } from 'store/services/auth';
import { z } from 'zod';

const schema = z.object({
  email: z.string().min(1, { message: 'Email is required' }).email({
    message: 'Must be a valid email',
  }),
  password: z.string().min(6, { message: 'Password must be atleast 6 characters' }),
});

type Values = z.infer<typeof schema>;

export const LoginForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
  } = useForm<Values>({
    resolver: zodResolver(schema),
  });

  const [login, { isLoading }] = useLoginMutation();

  const handleLogin = async (values: Values) => {
    try {
      schema.parse(values);
      const response = await login(values);
      if (isErrorResponseWithMessage(response)) {
        setError('email', { message: '' });
        setError('password', { message: response.error.data });
      }
    } catch {}
  };

  return (
    <form onSubmit={handleSubmit(handleLogin)}>
      <Box display="flex" flexDirection={'column'} height={256} maxWidth={400}>
        <TextField label="Username" {...register('email')} error={Boolean(errors.email)} helperText={errors.email?.message} sx={{ mb: 6 }} />
        <TextField
          label="Password"
          type="password"
          {...register('password')}
          error={Boolean(errors.password)}
          helperText={errors.password?.message}
          sx={{ mb: '4px' }}
        />
        <Link component={RouterLink} to="/password-reset">
          Forgot your password?
        </Link>
        <Button type="submit" disabled={isLoading} variant="contained" color="secondary" sx={{ mt: 'auto' }}>
          Log in
        </Button>
      </Box>
    </form>
  );
};
