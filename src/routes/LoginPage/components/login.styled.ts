import { styled } from '@mui/material/styles';

const GridLayout = styled('div')(({ theme }) => ({
  display: 'grid',
  gridTemplateColumns: '1fr auto minmax(auto, 50vw) 1fr',
  gridTemplateRows: '1fr auto 1fr',
  justifyItems: 'center',
  alignItems: 'center',
  padding: '40px',
  height: '100%',
  [theme.breakpoints.down('lg')]: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '16px',
  },
}));

const FormContainer = styled('div')(({ theme }) => ({
  maxWidth: '636px',
  paddingTop: '8%',
  gridRow: '2',
  gridColumn: '2',
  alignSelf: 'flex-start',
  [theme.breakpoints.down('xl')]: {
    paddingTop: 0,
  },
  [theme.breakpoints.down('lg')]: {
    marginTop: 'auto',
    alignSelf: 'initial',
  },
}));

const IllustrationContainer = styled('div')(({ theme }) => ({
  gridRow: '2',
  gridColumn: '3',
  alignSelf: 'center',
  img: {
    maxWidth: '100%',
  },
  [theme.breakpoints.down('lg')]: {
    display: 'none',
  },
}));

const Footer = styled('div')(({ theme }) => ({
  gridRow: '3',
  gridColumn: '2',
  justifySelf: 'flex-start',
  alignSelf: 'flex-end',
  display: 'flex',
  gap: '40px',
  [theme.breakpoints.down('lg')]: {
    alignSelf: 'initial',
    marginTop: 'auto',
  },
}));

const Styled = {
  GridLayout,
  FormContainer,
  IllustrationContainer,
  Footer,
};

export default Styled;
