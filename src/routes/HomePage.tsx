import { Alert, CircularProgress } from '@mui/material';
import { useGetNotificationsQuery } from 'store/services/notifications';

export const HomePage = () => {
  const { data: notifications, isLoading } = useGetNotificationsQuery();

  return isLoading ? <CircularProgress /> : <Alert severity="success">{notifications?.[0].description}</Alert>;
};
