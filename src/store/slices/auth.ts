import { createSlice } from '@reduxjs/toolkit';
import { authApi, type User } from '../services/auth';
import type { RootState } from '../store';

type State = { user: null | User; accessToken: string | null; isAuthenticated: boolean };

const initialState: State = {
  user: null,
  accessToken: null,
  isAuthenticated: false,
};

const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    logout: () => initialState,
  },
  extraReducers: (builder) => {
    builder.addMatcher(authApi.endpoints.login.matchFulfilled, (state, action) => {
      state.user = action.payload.user;
      state.accessToken = action.payload.accessToken;
      state.isAuthenticated = true;
    });
  },
});

export const selectIsAuthenticated = (state: RootState) => state.auth.isAuthenticated;

export const { logout } = slice.actions;

export default slice.reducer;
