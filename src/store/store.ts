import { configureStore } from '@reduxjs/toolkit';
import { api } from './services/api';
import auth from './slices/auth';

const store = configureStore({
  reducer: {
    [api.reducerPath]: api.reducer,
    auth,
  },
  devTools: process.env.NODE_ENV !== 'production',
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(api.middleware),
});

export default store;
// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
