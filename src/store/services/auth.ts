import { api } from './api';

export interface User {
  id: number;
  email: string;
}

export const authApi = api.injectEndpoints({
  endpoints: (build) => ({
    login: build.mutation<{ accessToken: string; user: User }, any>({
      query: (credentials: { email: string; password: string }) => ({
        url: 'login',
        method: 'POST',
        body: credentials,
      }),
      extraOptions: {
        retryCondition: () => false,
      },
    }),
  }),
});

export const { useLoginMutation } = authApi;
