import { api } from './api';

export type Notification = {
  id: string;
  title: string;
  description: string;
  created: string;
};

type NotificationsResponse = Notification[];

export const notificationsApi = api.injectEndpoints({
  endpoints: (build) => ({
    getNotifications: build.query<NotificationsResponse, void>({
      query: () => ({ url: 'notifications' }),
      providesTags: (result = []) => [...result.map(({ id }) => ({ type: 'Notifications', id } as const)), { type: 'Notifications' as const, id: 'LIST' }],
    }),
  }),
});

export const { useGetNotificationsQuery } = notificationsApi;
