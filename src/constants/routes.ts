export const ROUTES = {
  home: '/',
  welcome: '/welcome',
  login: '/login',
  passwordReset: '/password-reset',
};
