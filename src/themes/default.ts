import { createTheme } from '@mui/material/styles';

// A custom theme for this app
const theme = createTheme({
  typography: {
    fontFamily: ['"Noto Sans"', '"Ubuntu"', '"Helvetica Neue"', 'sans-serif'].join(','),
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        html: {
          height: '100%',
          scrollBehavior: 'smooth',
        },
        body: {
          WebkitFontSmoothing: 'antialiased',
          MozOsxFontSmoothing: 'grayscale',
          fontFamily: ['"Noto Sans"', '"Ubuntu"', '"Helvetica Neue"', 'sans-serif'].join(','),
          height: '100%',
          overflowY: 'scroll',
        },
        '#root': {
          height: '100%',
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        contained: ({ theme }) => ({
          fontSize: '1rem',
          fontWeight: 500,
          lineHeight: '1.5rem',
          letterSpacing: '0.15px',
          textTransform: 'none',
          boxShadow: 'none',
          ':hover': {
            boxShadow: '0px 1px 3px 1px rgba(28, 27, 31, 0.15), 0px 1px 2px 0px rgba(28, 27, 31, 0.30)',
            backgroundColor: theme.palette.secondary.main,
          },
        }),
      },
    },
    MuiLink: {
      styleOverrides: {
        root: ({ theme }) => ({
          color: theme.palette.text.secondary,
          fontWeight: 500,
          letterSpacing: '0.1px',
          textDecoration: 'none',
        }),
      },
    },
    MuiInputLabel: {
      styleOverrides: {
        outlined: ({ theme }) => ({
          position: 'absolute',
          top: '-24px',
          left: '16px',
          fontSize: '0.875rem',
          lineHeight: '1.25rem',
          transform: 'none',
          color: theme.palette.common.black,
        }),
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: ({ theme, ownerState }) => ({
          border: '1px solid',
          borderColor: ownerState.error ? theme.palette.error.main : theme.palette.common.black,
          borderRadius: 4,
          fontSize: '1rem',
          lineHeight: '1.5rem',
          letterSpacing: 0.5,
        }),
        input: {
          padding: '8px 16px',
        },
        notchedOutline: {
          display: 'none',
        },
      },
    },
    MuiTypography: {
      styleOverrides: {
        root: ({ theme }) => ({
          fontSize: '0.875rem',
          lineHeight: '1.25rem',
          color: theme.palette.text.primary,
        }),
        h1: ({ theme }) => ({
          fontSize: '3.5rem',
          fontWeight: 300,
          lineHeight: '4rem',
          letterSpacing: '-0.5px',
          [theme.breakpoints.down('xl')]: {
            fontSize: '2.5rem',
          },
          [theme.breakpoints.down('lg')]: {
            fontSize: '1.3rem',
          },
        }),
      },
    },
  },
  spacing: [0, 4, 8, 16, 24, 32, 40, 48, 56, 64, 72, 80],
  palette: {
    secondary: {
      main: '#BAA182',
    },
    text: {
      primary: '#1C1B1F',
      secondary: '#4D4D4D',
    },
    error: {
      main: '#BA1A1A',
    },
    common: {
      black: '#46416D',
    },
    background: {
      default: '#F9F9FA',
    },
  },
});

export default theme;
