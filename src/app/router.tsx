import { createBrowserRouter, Navigate, RouteObject } from 'react-router-dom';

import { GuestRoute } from '../components/GuestRoute';
import { ProtectedRoute } from '../components/ProtectedRoute';
import { ROUTES } from '../constants/routes';
import { ForgetPasswordPage } from '../routes/ForgetPasswordPage';
import { HomePage } from '../routes/HomePage';
import { LoginPage } from '../routes/LoginPage';
import { WelcomePage } from '../routes/WelcomePage';

const routerConfig: RouteObject[] = [
  {
    path: ROUTES.welcome,
    element: <WelcomePage />,
  },
  {
    path: ROUTES.login,
    element: (
      <GuestRoute>
        <LoginPage />
      </GuestRoute>
    ),
  },
  {
    path: ROUTES.passwordReset,
    element: (
      <GuestRoute>
        <ForgetPasswordPage />
      </GuestRoute>
    ),
  },
  {
    path: ROUTES.home,
    element: (
      <ProtectedRoute>
        <HomePage />
      </ProtectedRoute>
    ),
  },
  {
    path: '*',
    element: <Navigate to={ROUTES.welcome} />,
  },
];

const router = createBrowserRouter(routerConfig);

export default router;
